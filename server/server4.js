////////////
// States
const states = {
  IDLE: "Idle",
  LISTENING: "Listening",
  SPEAKING: "Speaking",
};

////////////
// Speaker(Optional)
// MacOS Issue
// [../deps/mpg123/src/output/coreaudio.c:81] warning: Didn't have any audio data in callback (buffer underflow)
// https://github.com/audiojs/audio-play/issues/15

// const Speaker = require("speaker");
const fs = require("fs");

const Readable = require("stream").Readable;
const pcmSpeaker = new Readable();
//let elevator_music_buffer = fs.readFileSync('/home/don/Documents/walkie/Walkie-Talkie_Project/Server_NodeJS/Mercedes AMG C63s Drive Moderate V2.wav');
let elevator_music_buffer = fs.readFileSync("music/zelda.wav");

// pcmSpeaker.bitDepth = 32;
// pcmSpeaker.channels = 1;
// pcmSpeaker.sampleRate = 16000;
// pcmSpeaker._read = bufRead;
// pcmSpeaker.pipe(new Speaker());
// function bufRead(buf) {
// 	if (buf instanceof Buffer) {
// 		pcmSpeaker.resume();
// 		pcmSpeaker.push(buf);
// 		// console.log(buf)
// 	} else if (buf == null) {
// 		pcmSpeaker.pause();
// 	}
// }

////////////
// WebSocket
const WebSocket = require("ws");
const WS_PORT = process.env.WS_PORT || 8888;
const WS_PORT2 = process.env.WS_PORT || 3000;

const wsServer = new WebSocket.Server(
  {
    port: WS_PORT,
    perMessageDeflate: {
      concurrencyLimit: 10,
      threshold: 1024,
    },
  },
  () => console.log(`[Server] WS server is listening at ${WS_PORT}`)
);

const wsServer2 = new WebSocket.Server(
  {
    port: WS_PORT2,
    perMessageDeflate: {
      concurrencyLimit: 10,
      threshold: 1024,
    },
  },
  () => console.log(`[Server2] WS server is listening at ${WS_PORT2}`)
);

//when the client connects
wsServer.on("connection", (ws, req) => {
  console.log("[Client] Connected!");
  ws.timestamp = getTimestamp();
  ws.id = wsServer.getUniqueID(ws.timestamp);
  ws.state = states.IDLE;
  ws.send(ws.timestamp);
  ws.on("message", (event) => {
    messageHandler(ws, event, elevator_music_buffer);
  });

  ws.on("cloWebSocketse", () => {
    //saveSessionToFile(); // Call saveSessionToFile() here
    console.log("[Client] Disconnected: " + ws.id);
  });
});

wsServer.getUniqueID = function (timestamp) {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return timestamp + "-" + s4();
};

//when the client connects
wsServer2.on("connection", (ws) => {
  console.log("[Client] Connected!" + ws.id);

  ws.timestamp = getTimestamp();
  ws.send(ws.timestamp);
  console.log(ws.timestamp);
  // console.log(wsServer.clients
  // console.log(elevator_music_buffer)
  // const bufferData = Buffer.from([0x48, 0x65, 0x6c, 0x6c, 0x6f]); // Example buffer
  const binaryData = elevator_music_buffer;

  // Split the file data into chunks

  // Code to be executed inside the loop

  const chunkSize = 1024;
  for (
    let offset = 0;
    offset < elevator_music_buffer.length;
    offset += chunkSize
  ) {
    const chunk = elevator_music_buffer.slice(offset, offset + chunkSize);
    console.log(chunk);
    sendData(chunk);

    // Send each chunk to the client
    //ws.send(chunk);
  }

  // sendData(binaryData)

  ws.on("close", () => {
    //saveSessionToFile(); // Call saveSessionToFile() here
    console.log("[Client] Disconnected: " + ws.id);
  });
});

function sendData(data) {
  console.log("send data has been called");
  wsServer2.clients.forEach(function each(client) {
    client.send(data);
  });
}

function messageHandler(ws, event, elevator_buff) {
  ws.timestamp = getTimestamp(); // sets up teh timestamp property

  //if the event is a buffer
  if (event instanceof Buffer) {
    if (event.length == 1) {
      console.log("[Client] Ping from: " + ws.id);
      // sendElevatorMusic_to_Clients(elevator_buff)
    } else if (event.length > 1) {
      // console.log("event is greatet than 1")
      console.log(event.length);
      sendDataClients(ws, event);
      // bufRead(event); // Resume Speaker Out
      ///bufRead(event)
    }
    //if the event is not a buffer
  } else {
    if (checkAllClientState()) {
      // determines if all the clients are in IDLE state- returns TRUE if all the clients are in IDLE state
      ws.state = states.SPEAKING; //If checkAllClientState() returns true, it means all clients are available for receiving new audio data, so the current WebSocket (ws) is set to the SPEAKING state.
      ws.send(event); // sending event message to all clients
      console.log("event message being sent:", event);
      // sendElevatorMusic_to_Clients(elevator_buff)
    } else {
      //otherwise at least one client is not in IDLE state
      ws.state = states.IDLE;
      console.log(" WS STATE SET TO IDLE:", ws.state);
      //bufRead(null); // Pause Speaker Out
      //sendElevatorMusic_to_Clients(ws, elevator_buff)
    }
    sendDataClients(ws, event);
  }
}

function sendDataClients(d_client, data, elev) {
  var timestamp = getTimestamp();
  wsServer.clients.forEach(function each(client) {
    if (d_client != client) {
      client.timestamp = timestamp;
      console.log(
        "[Client] Client Id: " + client.id + " Client State: " + client.state
      );
      if (client.state == states.IDLE) {
        // OTHER CLIENTS, IDLE -> LISTENING
        console.log("at this point client state is IDLE", client.state);
        if (data instanceof Buffer) {
          // NO ACTION REQUIRED
        } else {
          client.state = states.LISTENING;
          console.log("CLIENT STATE SET TO LISTENING:", client.state);

          client.send(timestamp);
        }
      } else {
        // OTHER CLIENTS, LISTENING -> IDLE
        if (data instanceof Buffer) {
          client.send(data);
          console.log("clients are being sent audio data:", data);
        } else {
          client.state = states.IDLE;
          console.log("CLIENT STATE SET TO IDLE:", client.state);

          client.send(timestamp);
        }
      }
    }
  });
}

function getTimestamp() {
  var last8digit_Timestamp = Date.now().toString().slice(-8);
  return parseInt(last8digit_Timestamp);
}

function checkAllClientState() {
  var result = false;
  wsServer.clients.forEach(function each(client) {
    if (client.state != states.IDLE) {
      result = false;
      return;
    }
    result = true;
  });
  return result;
}

function checkAlive() {
  console.log(
    "[Server] Total number of connected clients: " + wsServer.clients.size
  );
  wsServer.clients.forEach(function each(client) {
    if (client.state == states.IDLE) {
      console.log(
        "[Check Clinet Alive] Client Id: " +
          client.id +
          " Timestamp DIFF: " +
          (getTimestamp() - client.timestamp)
      );
      if (getTimestamp() - client.timestamp > 10000) {
        // 10sec
        console.log("[Client TimeOut] Client Id: " + client.id);
        client.close;
        wsServer.clients.delete(client);
      }
    }
  });
}

setInterval(function () {
  checkAlive();
}, 5000);
