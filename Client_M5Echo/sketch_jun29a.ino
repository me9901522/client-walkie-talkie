#include <driver/i2s.h>
#include <M5Atom.h>
#include <WiFi.h>
#include <ArduinoWebsockets.h>
#include "I2s_Setting.h"
#include "Const.h"

// #define CONFIG_I2S_BCK_PIN 19
// #define CONFIG_I2S_LRCK_PIN 33
// #define CONFIG_I2S_DATA_PIN 22
// #define CONFIG_I2S_DATA_IN_PIN 23

// #define SPEAKER_I2S_NUMBER I2S_NUM_0

// #define MODE_MIC 0
// #define MODE_SPK 1
// #define DATA_SIZE 1024

using namespace websockets;
WebsocketsClient client;

unsigned long timeStampOffset = 0;
unsigned long requestTimestamp;
unsigned long speakingStartTime;

int count = 0;
TaskHandle_t i2sReadTaskHandler = NULL;


void setup() {
  Serial.begin(115200);
  M5.begin(true, false, true);
  M5.dis.drawpix(0, CRGB(128, 128, 0));
  // InitI2SSpeakerOrMic(MODE_MIC);

  i2s_buff_init();
  start_to_connect();
  
  xTaskCreate(ping_task, "ping_task", 2048, NULL, 1, NULL);
}

void loop() {
  M5.update();
  if (client.available()) {
    client.poll();
  }

  if (M5.Btn.wasPressed())
  {
    if(states == Idle){
      Serial.println("Button idle");
      Serial.println(states);
      client.send(getStrTimestamp());
    }
    M5.dis.drawpix(0, CRGB(255, 0, 0));
  }
  if(M5.Btn.wasReleased())
  {
    if(states == Speaking){
      states = Idle;
      Serial.println("* IDLE Mode *");

      delay(100);
      if( i2sReadTaskHandler != NULL ){
        vTaskDelete( i2sReadTaskHandler );
        i2sReadTaskHandler = NULL;
      }
      
      delay(100);      
      client.send(getStrTimestamp());
      i2s_RX_uninst();
    }
    M5.dis.drawpix(0, CRGB(0, 255, 0));
  }
  //   data_offset = 0;
  //   M5.dis.drawpix(0, CRGB(128, 128, 0));
  //   size_t byte_read;

  //   while (1)
  //   {
  //     // if (data_offset < sizeof(microphonedata0)) {
  //     //   uint8_t recBuffer[DATA_SIZE];
  //     //   i2s_read(SPEAKER_I2S_NUMBER, (char*)(recBuffer), DATA_SIZE, &byte_read, (100 / portTICK_RATE_MS));
  //     //   for (int i = 0; i < byte_read; i += 4) {
  //     //     int16_t* val = (int16_t*)&recBuffer[i];
  //     //     int16_t* p = (int16_t*)&microphonedata0[data_offset];
  //     //     *p = *val;
  //     //     data_offset += 2;
  //     //   }
  //     // }

  //     M5.update();

  //     if (M5.Btn.isReleased()){
  //       states = Idle;
  //       Serial.println("* IDLE Mode *");

  //       delay(100);
  //       if (i2sReadTaskHandler != NULL)
  //       {
  //         vTaskDelete(i2sReadTaskHandler);
  //         i2sReadTaskHandler = NULL;
  //       }

  //       delay(100);
  //       client.send(getStrTimestamp());
  //       break;
  //     }
        
  //   }
  //   size_t bytes_written;
  //   InitI2SSpeakerOrMic(MODE_SPK);
  //   i2s_write(SPEAKER_I2S_NUMBER, microphonedata0, data_offset, &bytes_written, portMAX_DELAY);
  //   InitI2SSpeakerOrMic(MODE_MIC);
  // }
  
}



void onEventsCallback(WebsocketsEvent event, String data)
{
  if (event == WebsocketsEvent::ConnectionClosed)
  {
    ESP.restart();
  }
}

void onMessageCallback(WebsocketsMessage message)
{
  int msgLength = message.length();
  if (message.type() == MessageType::Binary)
  {
    if (states == Listening && msgLength > 0)
    {
      i2s_write_data((char *)message.c_str(), msgLength);
      Serial.println("Write data here");
    }
  }
  else if (message.type() == MessageType::Text)
  {
    unsigned long timeResponse = message.data().toInt();
    actionCommand(timeResponse);
  }
}

// void InitI2SSpeakerOrMic(int mode)
// {
//   esp_err_t err = ESP_OK;

//   i2s_driver_uninstall(SPEAKER_I2S_NUMBER);
//   i2s_config_t i2s_config = {
//     .mode = (i2s_mode_t)(I2S_MODE_MASTER),
//     .sample_rate = 16000,
//     .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT, // is fixed at 12bit, stereo, MSB
//     .channel_format = I2S_CHANNEL_FMT_ALL_RIGHT,
//     .communication_format = I2S_COMM_FORMAT_STAND_I2S,
//     .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
//     .dma_buf_count = 6,
//     .dma_buf_len = 60,
//   };
//   if (mode == MODE_MIC)
//   {
//     i2s_config.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX | I2S_MODE_PDM);
//   }
//   else
//   {
//     i2s_config.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX);
//     i2s_config.use_apll = false;
//     i2s_config.tx_desc_auto_clear = true;
//   }

//   err += i2s_driver_install(SPEAKER_I2S_NUMBER, &i2s_config, 0, NULL);
//   i2s_pin_config_t tx_pin_config;

//   tx_pin_config.mck_io_num = GPIO_NUM_0;
//   tx_pin_config.bck_io_num = CONFIG_I2S_BCK_PIN;
//   tx_pin_config.ws_io_num = CONFIG_I2S_LRCK_PIN;
//   tx_pin_config.data_out_num = CONFIG_I2S_DATA_PIN;
//   tx_pin_config.data_in_num = CONFIG_I2S_DATA_IN_PIN;

//   //Serial.println("Init i2s_set_pin");
//   err += i2s_set_pin(SPEAKER_I2S_NUMBER, &tx_pin_config);
//   //Serial.println("Init i2s_set_clk");
//   if(mode != MODE_MIC){
//     err += i2s_set_clk(SPEAKER_I2S_NUMBER, 16000, I2S_BITS_PER_SAMPLE_16BIT, I2S_CHANNEL_MONO);
//   }
// }

void start_to_connect()
{
  WiFi.begin(ssid, password);
  //  print_log_screen("- WiFi Connecting");
  Serial.println("- WiFi Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("+ WiFi Connected");
  Serial.println("- Socket Connecting");

  client.onMessage(onMessageCallback);
  client.onEvent(onEventsCallback);
  while (!client.connect(websockets_server_host, websockets_server_port, "/"))
  { // CONNECTION TO WEBSOCKET IS MADE HERE
    delay(500);
    Serial.print(".");
  }

  Serial.println("+ Socket Connected");
}



void actionCommand(unsigned long timestamp)
{
  if (timeStampOffset == 0)
  {
    unsigned long currentTimestamp = millis();
    timeStampOffset = currentTimestamp >= timestamp ? currentTimestamp - timestamp : timestamp - currentTimestamp;
    return;
  }

  if (requestTimestamp == timestamp && states == Idle)
  {
    states = Speaking;
    speakingStartTime = millis(); // Start the Speaking mode timer
    Serial.println("* Speaking Mode *");
    i2s_RX_init();
    // led.turnON();
    // digitalWrite(LED_PIN, HIGH);
    M5.dis.drawpix(0, CRGB(255, 0, 0));

    xTaskCreate(i2s_read_task, "i2s_read_task", 4096, NULL, 1, &i2sReadTaskHandler);
  }
  else if (requestTimestamp != timestamp)
  {
    if (states == Idle)
    {
      states = Listening;
      i2s_TX_init();
      Serial.println("* Listening Mode *");
      M5.dis.drawpix(0, CRGB(0, 255, 0));
    }
    else if (states == Listening)
    {
      states = Idle;
      i2s_TX_uninst();
      Serial.println("* IDLE Mode *");
      M5.dis.drawpix(0, CRGB(0, 0, 255));
      // led.turnOFF();
      // digitalWrite(LED_PIN, LOW);
    }
  }
}

static void i2s_read_task(void *arg)
{
  while (1)
  {
    i2s_read_data();
    client.sendBinary((const char *)flash_write_buff, I2S_READ_LEN);
  }
}

static void ping_task(void *arg)
{
  char dummy_buffer[1];
  while (1)
  {
    if (states == Idle)
    {
      client.sendBinary(dummy_buffer, 1);
    }
    vTaskDelay(1000);
  }
}

String getStrTimestamp()
{
  requestTimestamp = millis() + timeStampOffset;
  Serial.println(String(requestTimestamp));
  return (String)requestTimestamp;
}